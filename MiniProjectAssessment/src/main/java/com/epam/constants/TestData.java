package com.epam.constants;

public class TestData {
    public static final String CREATE_USER_API_URI = "https://demoqa.com/Account/v1/User";
    public static final String BOOKS_API_URI = "https://demoqa.com/BookStore/v1/Books";
    public static final String LOGIN_PAGE_URL = "https://demoqa.com/login";
    public static final String BOOKS_PAGE_URL = " https://demoqa.com/books";
}
