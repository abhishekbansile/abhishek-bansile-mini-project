package com.epam.constants;

import java.time.Duration;
public class Constants {
    public static final String CHROME_BROWSER = "webdriver.chrome.driver";
    public static final String CHROME_BROWSER_PATH = "src/test/resources/Drivers/chromedriver(113).exe";
    public static final String EDGE_BROWSER = "webdriver.edge.driver";
    public static final String EDGE_BROWSER_PATH = "src/test/resources/Drivers/msedgedriver.exe";
    public static final String CONFIGURATION_FILE_PATH = "src/test/resources/config.properties";
    public static final Duration IMPLICIT_WAIT_DURATION = Duration.ofSeconds(20);
    public static final Duration EXPLICIT_WAIT_DURATION = Duration.ofSeconds(20);
    public static final String AD_BLOCKER_EXTENSION = "src/test/resources/addBlockerExtension.crx";
}
