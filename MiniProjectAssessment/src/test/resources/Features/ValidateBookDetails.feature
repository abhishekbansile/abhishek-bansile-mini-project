Feature: Validate book details from API and UI both
  @Positive @API @UI
  Scenario: Validate book details from API and UI both
    When user send a GET request to Books API
    Then user validate status code as 200 and captured the details of all the books
    When user navigate to demoqa books page
    Then user validates the captured book details from UI