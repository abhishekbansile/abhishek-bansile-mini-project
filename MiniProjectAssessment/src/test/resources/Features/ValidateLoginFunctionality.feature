Feature: Validate demoqa login functionality

  @Positive @API @UI
  Scenario: Validate login functionality from API and UI both with validate username and password
    When user send a POST request to User API and create user login using following details
      | username | abhishekbansile93    |
      | password | Abhishek@123            |
    Then status code should be 201
    And user validate response body with input data
    When user navigate to demoqa login page
    And user enters same username and password used for API Post request
    And user click on login button
    Then user validate username on the Books Dashboard page

  @Negative @API
  Scenario: Validate login functionality from API with Already UserExists username
    When user send a POST request to User API and create user login using following details
      | username | abhishek14    |
      | password | Abhishek@123  |
    Then status code should be 406
    And user validate response body with following expected data
      | code    | 1204         |
      | message | User exists! |

  @Negative @API
  Scenario Outline: Validate login functionality from API with invalid password
    When user send a POST request to User API and create user login using following details
      | username | abhishekbansile004 |
      | password | <password>   |
    Then status code should be 400
    And user validate response body with following expected data
      | code    | 1300    |
      | message | Passwords must have at least one non alphanumeric character, one digit ('0'-'9'), one uppercase ('A'-'Z'), one lowercase ('a'-'z'), one special character and Password must be eight characters or longer. |
    Examples:
      | password     |
      | abhishek     |
      | ABHISHEK     |
      | abhishek123  |
      | ABHISHEK123  |
      | abhishek@123 |
      | ABHISHEK@123 |
  @Negative @UI
  Scenario Outline: validate login functionality from UI with incorrect Login credential
    When user navigate to demoqa login page
    And user enters following username and password on login page
    | username | <username> |
    | password | <password> |
    And user click on login button
    Then the error message should be displayed as "Invalid username or password!"
    Examples:
      | username | password |
      | Abhishek09 | Abhishek123 |
      | Abhishek098 | Abhishek |