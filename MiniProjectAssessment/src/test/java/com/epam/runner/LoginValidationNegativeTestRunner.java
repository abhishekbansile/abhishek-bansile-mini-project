package com.epam.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
@CucumberOptions(plugin = "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
        glue = "com.epam", features = "src/test/resources/Features/ValidateLoginFunctionality.feature",
        tags = "@Negative")
public class LoginValidationNegativeTestRunner extends AbstractTestNGCucumberTests{
}
