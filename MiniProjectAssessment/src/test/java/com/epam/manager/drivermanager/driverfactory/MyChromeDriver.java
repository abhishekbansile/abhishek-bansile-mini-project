package com.epam.manager.drivermanager.driverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

import static com.epam.constants.Constants.*;

public class MyChromeDriver implements MyWebDriver{
    @Override
    public WebDriver getDriver() {
        System.setProperty(CHROME_BROWSER, CHROME_BROWSER_PATH);
        ChromeOptions options = new ChromeOptions();
        options.addExtensions(new File(AD_BLOCKER_EXTENSION));
        options.addArguments("--remote-allow-origins=*");
        return new ChromeDriver(options);
    }
}
