package com.epam.manager.drivermanager.driverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirefoxDriver implements MyWebDriver {
    @Override
    public WebDriver getDriver() {
        return new FirefoxDriver();
    }
}
