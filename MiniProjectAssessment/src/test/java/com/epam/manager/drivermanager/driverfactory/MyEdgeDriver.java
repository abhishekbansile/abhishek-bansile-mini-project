package com.epam.manager.drivermanager.driverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import static com.epam.constants.Constants.*;

public class MyEdgeDriver implements MyWebDriver {

    @Override
    public WebDriver getDriver() {
        System.setProperty(EDGE_BROWSER, EDGE_BROWSER_PATH);
        return new EdgeDriver();
    }
}
