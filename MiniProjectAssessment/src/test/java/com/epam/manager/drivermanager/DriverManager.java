package com.epam.manager.drivermanager;

import com.epam.exceptions.BrowserNotFoundException;
import com.epam.manager.drivermanager.driverfactory.*;
import com.epam.utility.FileReader;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static com.epam.constants.Constants.CONFIGURATION_FILE_PATH;

public class DriverManager {
    private static WebDriver driver;
    private DriverManager(){

    }
    public static WebDriver getDriver() throws BrowserNotFoundException, IOException {
        if(driver==null){
            synchronized (DriverManager.class){
                if(driver==null){
                    initializeDriver();
                }
            }
        }
        return driver;
    }

    private static void initializeDriver() throws BrowserNotFoundException, IOException {
        switch (FileReader.readPropertiesFile(CONFIGURATION_FILE_PATH).getProperty("browser").toUpperCase()){
            case "CHROME":
                driver = new MyChromeDriver().getDriver();
                break;
            case "FIREFOX":
                driver = new MyFirefoxDriver().getDriver();
                break;
            case "EDGE":
                driver = new MyEdgeDriver().getDriver();
                break;
            default: throw new BrowserNotFoundException("Provide correct browser name in configuration file - "+CONFIGURATION_FILE_PATH);
        }
    }
    public static void closeDriver(){
        driver.quit();
        driver = null;
    }
}
