package com.epam.manager.drivermanager.driverfactory;

import org.openqa.selenium.WebDriver;

public interface MyWebDriver {
    WebDriver getDriver();
}
