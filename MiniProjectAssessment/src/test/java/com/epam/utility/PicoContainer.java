package com.epam.utility;

import com.epam.pages.BooksPage;
import com.epam.pages.LoginPage;
import com.epam.pages.ProfilePage;
import org.openqa.selenium.WebDriver;

public class PicoContainer {
    private WebDriver driver;
    private LoginPage loginPage;
    private ProfilePage profilePage;
    private BooksPage booksPage;
    public void initializePages(){
        loginPage = new LoginPage(driver);
        profilePage = new ProfilePage(driver);
        booksPage = new BooksPage(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage getLoginPage() {
        return loginPage;
    }

    public ProfilePage getProfilePage() {
        return profilePage;
    }

    public BooksPage getBooksPage() {
        return booksPage;
    }
}
