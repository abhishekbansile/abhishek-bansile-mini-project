package com.epam.stepdef;

import com.epam.pages.LoginPage;
import com.epam.pages.ProfilePage;
import com.epam.utility.PicoContainer;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import java.util.Map;
import static com.epam.constants.TestData.CREATE_USER_API_URI;
import static com.epam.constants.TestData.LOGIN_PAGE_URL;
import static io.restassured.RestAssured.given;

public class LoginValidationStepDef {
    private PicoContainer picoContainer;
    private LoginPage loginPage;
    private ProfilePage profilePage;
    private JSONObject userAccountDetails;
    private Response userCreationResponse;
    public LoginValidationStepDef(PicoContainer picoContainer) {
        this.picoContainer = picoContainer;
        loginPage = picoContainer.getLoginPage();
        profilePage = picoContainer.getProfilePage();
    }
    @When("user send a POST request to User API and create user login using following details")
    public void userSendAPOSTRequestToUserAPIAndCreateUserLoginUsingFollowingDetails(DataTable dataTable) {
        Map<String, String> accountDetails = dataTable.asMap();
        userAccountDetails = new JSONObject();
        userAccountDetails.put("userName", accountDetails.get("username"));
        userAccountDetails.put("password", accountDetails.get("password"));
        userCreationResponse = given().contentType(ContentType.JSON).body(userAccountDetails.toString()).post(CREATE_USER_API_URI);
    }
    @Then("status code should be {int}")
    public void statusCodeShouldBe(int expectedStatusCode) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(userCreationResponse.getStatusCode(), expectedStatusCode);
        softAssert.assertAll();
    }
    @When("user navigate to demoqa login page")
    public void userNavigateToDemoqaLoginPage() {
        picoContainer.getDriver().get(LOGIN_PAGE_URL);
    }
    @And("user enters same username and password used for API Post request")
    public void userEntersSameUsernameAndPasswordUsedForAPIPostRequest() {
        loginPage.enterUsername((String) userAccountDetails.get("userName")).enterPassword((String) userAccountDetails.get("password"));
    }

    @And("user click on login button")
    public void userClickOnLoginButton() {
        loginPage.clickOnLogin();
    }

    @Then("user validate username on the Books Dashboard page")
    public void userValidateUsernameOnTheBooksDashboardPage() {
        Assert.assertEquals(profilePage.getUsername(), userAccountDetails.get("userName"));
    }

    @And("user validate response body with input data")
    public void userValidateResponseBodyWithInputData() {
        Assert.assertEquals(userCreationResponse.path("username"), userAccountDetails.get("userName"));
    }

    @And("user validate response body with following expected data")
    public void userValidateResponseBodyWithFollowingExpectedData(DataTable dataTable) {
        Map<String, String> expectedResponseBody = dataTable.asMap();
        SoftAssert softAssert = new SoftAssert();
        JsonPath jsonPath = userCreationResponse.jsonPath();
        softAssert.assertEquals(expectedResponseBody.get("code"), jsonPath.getString("code"));
        softAssert.assertTrue(expectedResponseBody.get("message").equalsIgnoreCase(jsonPath.getString("message")));
        softAssert.assertAll();
    }

    @And("user enters following username and password on login page")
    public void userEntersFollowingUsernameAndPasswordOnLoginPage(DataTable dataTable) {
        Map<String, String> loginDetails = dataTable.asMap();
        loginPage.enterUsername(loginDetails.get("username")).enterPassword(loginDetails.get("password"));
    }

    @Then("the error message should be displayed as {string}")
    public void theErrorMessageShouldBeDisplayedAs(String errorMessage) {
        Assert.assertTrue(loginPage.getErrorMessage().equalsIgnoreCase(errorMessage));
    }
}
