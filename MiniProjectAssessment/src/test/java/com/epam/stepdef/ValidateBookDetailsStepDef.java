package com.epam.stepdef;

import com.epam.pages.BooksPage;
import com.epam.pojo.Book;
import com.epam.utility.PicoContainer;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import java.util.List;
import static com.epam.constants.TestData.*;
import static io.restassured.RestAssured.given;

public class ValidateBookDetailsStepDef {
    private BooksPage booksPage;
    private PicoContainer picoContainer;
    private Response bookResponse;
    private List<Book> bookList;
    public ValidateBookDetailsStepDef(PicoContainer picoContainer){
        this.picoContainer = picoContainer;
        this.booksPage = picoContainer.getBooksPage();
    }
    @When("user send a GET request to Books API")
    public void userSendAGETRequestToBooksAPI() {
        bookResponse = given().get(BOOKS_API_URI);
    }

    @Then("user validate status code as {int} and captured the details of all the books")
    public void userValidateStatusCodeAsAndCapturedTheDetailsOfAllTheBooks(int statusCode) {
        Assert.assertEquals(bookResponse.getStatusCode(), statusCode);
        bookList = bookResponse.jsonPath().getList("books", Book.class);
    }

    @When("user navigate to demoqa books page")
    public void userNavigateToDemoqaBooksPage() {
        picoContainer.getDriver().get(BOOKS_PAGE_URL);
    }
    @Then("user validates the captured book details from UI")
    public void userValidatesTheCapturedBookDetailsFromUI() throws InterruptedException {
        for(Book book : bookList){
            SoftAssert softAssert = new SoftAssert();
            booksPage.searchBooks(book.getTitle()).clickOnFirstBook();
            softAssert.assertTrue(booksPage.getIsbnNumber().equals(book.getIsbn()));
            softAssert.assertTrue(booksPage.getTitle().equals(book.getTitle()));
            softAssert.assertTrue(booksPage.getSubTitle().equals(book.getSubTitle()));
            softAssert.assertTrue(booksPage.getAuthor().equals(book.getAuthor()));
            softAssert.assertTrue(booksPage.getPublisher().equals(book.getPublisher()));
            softAssert.assertEquals(booksPage.getTotalPages(), (book.getPages()));
            softAssert.assertTrue(booksPage.getDescription().equals(book.getDescription()));
            softAssert.assertTrue(booksPage.getWebsite().equals(book.getWebsite()));
            picoContainer.getDriver().navigate().back();
            softAssert.assertAll();
        }
    }
}
