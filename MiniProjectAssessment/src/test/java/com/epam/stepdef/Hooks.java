package com.epam.stepdef;

import com.epam.exceptions.BrowserNotFoundException;
import com.epam.manager.drivermanager.DriverManager;
import com.epam.utility.PicoContainer;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;

import static com.epam.constants.Constants.IMPLICIT_WAIT_DURATION;

public class Hooks {
    private static final Logger LOGGER = LogManager.getLogger();
    private PicoContainer picoContainer;
    public Hooks(PicoContainer picoContainer){
        this.picoContainer = picoContainer;
    }
    @Before("@UI")
    public void setUp() throws BrowserNotFoundException, IOException {
        picoContainer.setDriver(DriverManager.getDriver());
        LOGGER.info("Driver Initialized");
        picoContainer.initializePages();
        picoContainer.getDriver().manage().window().maximize();
        picoContainer.getDriver().manage().timeouts().implicitlyWait(IMPLICIT_WAIT_DURATION);
        LOGGER.info("Browser Launched");
    }
    @After("@UI")
    public void tearDown(){
        DriverManager.closeDriver();
        LOGGER.info("Driver Closed");
    }
    @AfterStep
    public void afterStep(Scenario scenario) throws IOException {
        if(scenario.isFailed()){
            LOGGER.info("Scenario: " + scenario.getName() + " failed");
            String fileName = scenario.getName().replaceAll(" ", "_");
            File screenCapture = ((TakesScreenshot)picoContainer.getDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenCapture, new File(".//target/screenshots/"+fileName +".png"));
            scenario.attach(screenCapture.getAbsolutePath(), "image/path", scenario.getName());
        }
    }
}
