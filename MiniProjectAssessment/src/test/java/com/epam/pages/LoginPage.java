package com.epam.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.constants.Constants.EXPLICIT_WAIT_DURATION;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver) {
        super(driver);
    }
    protected WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_DURATION);
    private static final Logger LOGGER = LogManager.getLogger();

    @FindBy(id = "userName")
    private WebElement usernameField;
    @FindBy(id = "password")
    private WebElement passwordField;
    @FindBy(id = "login")
    private WebElement loginButton;
    @FindBy(id= "name")
    private WebElement errorMessage;

    public LoginPage enterUsername(String username){
        usernameField.sendKeys(username);
        return this;
    }
    public LoginPage enterPassword(String password){
        passwordField.sendKeys(password);
        return this;
    }
    public ProfilePage clickOnLogin(){
        wait.until(ExpectedConditions.elementToBeClickable(loginButton)).click();
        return new ProfilePage(driver);
    }
    public String getErrorMessage(){
        return errorMessage.getText().trim();
    }
}
