package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProfilePage extends BasePage {
    public ProfilePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(id = "userName-value")
    private WebElement usernameLabel;

    public String getUsername(){
        return usernameLabel.getText().trim();
    }
}
