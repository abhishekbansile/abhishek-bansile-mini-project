package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static com.epam.constants.Constants.EXPLICIT_WAIT_DURATION;

public class BooksPage extends BasePage {
    public BooksPage(WebDriver driver) {
        super(driver);
    }
    protected WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_DURATION);
    @FindBy(id = "searchBox")
    private WebElement searchBox;
    @FindBy(xpath = "//div[@class='rt-tr-group'][1]//a")
    private WebElement firstBook;
    @FindBy(xpath = "//div[@id='ISBN-wrapper']/div[2]/label")
    private WebElement isbnNumber;
    @FindBy(xpath = "//div[@id='subtitle-wrapper']/div[2]/label")
    private WebElement subTitle;
    @FindBy(xpath = "//div[@id='title-wrapper']/div[2]/label")
    private WebElement title;
    @FindBy(xpath = "//div[@id='author-wrapper']/div[2]/label")
    private WebElement author;
    @FindBy(xpath = "//div[@id='publisher-wrapper']/div[2]/label")
    private WebElement publisher;
    @FindBy(xpath = "//div[@id='pages-wrapper']/div[2]/label")
    private WebElement totalPages;
    @FindBy(xpath = "//div[@id='description-wrapper']/div[2]/label")
    private WebElement description;
    @FindBy(xpath = "//div[@id='website-wrapper']/div[2]/label")
    private WebElement website;
    public BooksPage searchBooks(String bookName){
        searchBox.clear();
        searchBox.sendKeys(bookName);
        return this;
    }
    public BooksPage clickOnFirstBook() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(firstBook)).click();
        return this;
    }
    public String getIsbnNumber(){
        return isbnNumber.getText().trim();
    }
    public String getTitle(){
        return title.getText().trim();
    }
    public String getSubTitle(){
        return subTitle.getText().trim();
    }
    public String getAuthor(){
        return author.getText().trim();
    }
    public String getPublisher(){
        return publisher.getText().trim();
    }
    public int getTotalPages(){
        return Integer.parseInt(totalPages.getText().trim());
    }
    public String getDescription(){
        return description.getText().trim();
    }
    public String getWebsite(){
        return website.getText().trim();
    }
}
